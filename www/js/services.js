angular.module('starter.services', [])
.factory('Test', function() {
  var title = "QuestName";
  var results = [{
      point: 1,
      content: "test1"
    },{
      point: 3,
      content: "test2"
    },{
      point: 100,
      content: "test3"
    }];

  var quest = [{
    id: 0,
    questTitle: "quest1",
    answers: [
        { 
          id: 0,
          title: "answer 1",
          points: 0
        },
        { 
          id: 1,
          title: "answer 2",
          points: 1
        },
        { 
          id: 2,
          title: "answer 3",
          points: 2
        },
        { 
          id: 3,
          title: "answer 4",
          points: 3
        }
      ]
    },{
    id: 1,
    questTitle: "quest2",
    answers: [
        { 
          id: 0,
          title: "answer 2.1",
          points: 0
        },
        { 
          id: 1,
          title: "answer 2.2",
          points: 1
        },
        { 
          id: 2,
          title: "answer 2.3",
          points: 2
        },
        { 
          id: 3,
          title: "answer 2.4",
          points: 3
        }
      ]
    },{
    id: 2,
    questTitle: "quest3",
    answers: [
        { 
          id: 0,
          title: "answer 3.1",
          points: 0
        },
        { 
          id: 1,
          title: "answer 3.2",
          points: 1
        },
        { 
          id: 2,
          title: "answer 3.3",
          points: 2
        },
        { 
          id: 3,
          title: "answer 3.4",
          points: 3
        }
      ]
    }];

  return {
    all: function(catId) {
        //TODO запрос на получение квеста
          return quest;
    },
    get: function(catId) {
      for (var i = 0; i < quest.length; i++) {
        if (quest[i].id === parseInt(catId)) {
          return quest[i];
        }
      }
      return null;
    },
    getResult: function(point) {
      for (var i = 0; i < results.length; i++) {
        if ( parseInt(point) > results[i].point && parseInt(point) <= results[i+1].point) {
          return results[i];
        }
      }
      return null;
    },
    getTitle: function() {
      return title;
    },
  };
})
.factory('Category', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var categoryes = [{
    id: 0,
    name: 'Тесты IQ',
    lastText: 'You on your way?',
    face: 'img/ben.png',
    subCat: [{
      id: 4,
      name: 'Тест IQ 1',
      lastText: 'Hey, it\'s me',
      face: 'img/max.png'
    },{
      id: 5,
      name: 'Тест IQ 2',
      lastText: 'Hey, it\'s me',
      face: 'img/max.png'
    }]
  }, {
    id: 1,
    name: 'Забавные',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Образовательные',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Психологические',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }];

  return {
    all: function() {
      return categoryes;
    },
    getCat: function(catId) {
      for (var i = 0; i < categoryes.length; i++) {
        if (categoryes[i].id === parseInt(catId)) {
          console.log(categoryes[i].subCat);
          return categoryes[i].subCat;
        }
      }
      return null;
    },
    remove: function(cat) {
      categoryes.splice(categoryes.indexOf(chat), 1);
    },
    get: function(catId) {
      for (var i = 0; i < categoryes.length; i++) {
        if (categoryes[i].id === parseInt(catId)) {
          return categoryes[i];
        }
      }
      return null;
    }
  };
});
