angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, Category) {
  $scope.categoryes = Category.all();
})

.controller('SubCategory', function($scope, $stateParams, Category) {
  $scope.testId = $stateParams.catId;
  $scope.subcats = Category.getCat($stateParams.catId);
  console.log("$scope.subcats",$scope.subcats);
})
.controller('TestCtrl', function($scope, $stateParams, Test) {
  var points = 0;  
  $scope.step = 0;
  $scope.testName = Test.getTitle();
  $scope.quests = Test.all($stateParams.catId);
  
  $scope.questTitle = $scope.quests[$scope.step].questTitle;
  $scope.answers = $scope.quests[$scope.step].answers;

  $scope.select = function(id, step, point) {
    $scope.showResult = false;
    points += point;
    step++;
      console.log("points", points);
    if($scope.quests.length <= step){
      //TODO result
      var result = Test.getResult(points);
      $scope.resultContent = result.content;
      $scope.showResult = true;
      console.log("$scope.result", $scope.resultContent);
    }else{
      $scope.step = step;
      $scope.questTitle = $scope.quests[step].questTitle;
      $scope.answers = $scope.quests[step].answers;
    }
  };

})
.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});